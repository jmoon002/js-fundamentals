/**
 * Serperates a string and places it into a list.
 * @param {string} str 
 * @param {string} seperator 
 * @return {array}
 */
function split(str, seperator) {
    let list = []
    let otherStr = ''
    for (let i = 0; i != str.length; i++){
        let char = str.charAt(i)
        if (char != seperator) {
            otherStr = otherStr + char
        }
        else {
            list.push(otherStr)
            otherStr = ''
        }
    }
    list.push(otherStr)
    return list
}