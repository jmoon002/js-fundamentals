/**
 * This function grabs the fibonacci number you want from a list
 * @param {number} n 
 */
function fibonacci(n) {
    let list = [0, 1]

    for (let i = 0; i != n - 2; i++) {
        let o = list[i] + list[i + 1]
        list.push(o) 
    }
    return {number: list[n - 1], list: list}
};

console.log(fibonacci(5))